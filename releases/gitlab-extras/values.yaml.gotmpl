---

{{ $namespace := .Values | getOrNil "gitlab_extras.namespace" | default "gitlab" }}

resources:
{{- if .Values | getOrNil "gitlab.kas.enabled" | default false }}
  - apiVersion: networking.gke.io/v1
    kind: ManagedCertificate
    metadata:
      name: {{ .Values.kas.managed_cert_name }}
      namespace: {{ $namespace }}
    spec:
      domains:
        - {{ .Values.kas.domain }}

  - apiVersion: cloud.google.com/v1
    kind: BackendConfig
    metadata:
      name: kas-http-backendconfig
      namespace: {{ $namespace }}
    spec:
      healthCheck:
        port: 8151
        type: HTTP
        requestPath: /liveness
        checkIntervalSec: 5
        timeoutSec: 3
      timeoutSec: 1830
      securityPolicy:
        name: "kas-ingress-policy"

  - apiVersion: networking.gke.io/v1beta1
    kind: FrontendConfig
    metadata:
      name: kas-http-frontendconfig
      namespace: {{ $namespace }}
    spec:
      redirectToHttps:
        enabled: true
{{- end }}

{{- if .Values | getOrNil "gitlab_extras.api_no_proxy" | default false }}
  - apiVersion: networking.k8s.io/v1beta1
    kind: Ingress
    metadata:
      annotations:
        nginx.ingress.kubernetes.io/use-regex: "true"
        kubernetes.io/ingress.class: gitlab-nginx
        kubernetes.io/ingress.provider: nginx
        nginx.ingress.kubernetes.io/proxy-body-size: "0"
        nginx.ingress.kubernetes.io/proxy-buffering: "off"
        nginx.ingress.kubernetes.io/proxy-buffers-number: "8"
        nginx.ingress.kubernetes.io/proxy-connect-timeout: "300"
        nginx.ingress.kubernetes.io/proxy-max-temp-file-size: "0"
        nginx.ingress.kubernetes.io/proxy-read-timeout: "3600"
        nginx.ingress.kubernetes.io/proxy-request-buffering: "off"
        nginx.ingress.kubernetes.io/service-upstream: "true"
{{- if (.Values | getOrNil "gitlab_internal_lb_dns_address") | default false }}
        nginx.ingress.kubernetes.io/server-alias: {{ .Values | getOrNil "gitlab_internal_lb_dns_address" }}
{{- end }}
      labels:
        app: webservice
        gitlab.com/webservice-name: api
        gitlab.com/webservice-ingress: proxy-off
        shard: default
        stage: {{ if eq $namespace "gitlab-cny" }}"cny"{{ else }}"main"{{ end }}
        tier: sv
        type: api
      name: gitlab-webservice-api-proxy-off
      namespace: {{ $namespace }}
    spec:
      rules:
      - host: {{ .Values | getOrNil "gitlab_domain" }}
        http:
          paths:
          - backend:
              serviceName: {{ $namespace }}-webservice-api
              servicePort: 8181
            path: /api/v[0..9]/jobs/[0-9]+/artifacts$
            pathType: ImplementationSpecific
      tls:
      - hosts:
        - {{ .Values | getOrNil "gitlab_domain" | default "" }}
        secretName: gitlab-wildcard-tls
{{- end }}

{{- if .Values | getOrNil "gitlab_extras.ingress_readiness" | default false }}
  - apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      annotations:
        kubernetes.io/ingress.class: gitlab-nginx
        kubernetes.io/ingress.provider: nginx
        nginx.ingress.kubernetes.io/service-upstream: "true"
        nginx.ingress.kubernetes.io/rewrite-target: "/$2"
{{- if (.Values | getOrNil "gitlab_internal_lb_dns_address") | default false }}
        nginx.ingress.kubernetes.io/server-alias: {{ .Values | getOrNil "gitlab_internal_lb_dns_address" }}
{{- end }}
      labels:
        app: webservice
        shard: default
        stage: {{ default ( .Values | getOrNil "stage" ) "main" }}
        tier: sv
      name: gitlab-readiness
      namespace: {{ $namespace }}
    spec:
      rules:
      - host: {{ .Values | getOrNil "gitlab_domain" }}
        http:
          paths:
          - backend:
              service:
                name: {{ $namespace }}-webservice-api
                port:
                  number: 8181
            path: /-/k8s/api(/|$)(.*)
            pathType: ImplementationSpecific
          - backend:
              service:
                name: {{ $namespace }}-webservice-web
                port:
                  number: 8181
            path: /-/k8s/web(/|$)(.*)
            pathType: ImplementationSpecific
      tls:
      - hosts:
        - {{ .Values | getOrNil "gitlab_domain" | default "" }}
        secretName: gitlab-wildcard-tls
{{- end }}
