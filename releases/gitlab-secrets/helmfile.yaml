---
bases:
  - "../../bases/helmDefaults.yaml"
  - "../../bases/environments.yaml"

---
{{ $namespace := .Environment.Values | getOrNil "gitlab_secrets_namespace" | default "gitlab" }}
repositories:
# Kubernetes incubator repo of helm charts
- name: "kubernetes-incubator"
  url: "https://charts.helm.sh/incubator"

releases:
- name:  {{ .Environment.Values | getOrNil "gitlab_secrets_release_name" | default "gitlab-secrets" }}
  chart: "kubernetes-incubator/raw"
  namespace: {{ $namespace }}
  version: "0.2.3"
  installed: {{ env "GITLAB_SECRETS_INSTALLED" | default "true" }}
  values:
{{- $env_prefix := .Environment.Values | getOrNil "env_prefix" | default .Environment.Name }}
{{- $gkms_source := .Environment.Values | getOrNil "gitlab_secrets_gkms_source" | default $env_prefix }}
{{- $gkms_omnibus_secrets := exec "bash" (list "-c" (print "gsutil cat gs://gitlab-" $gkms_source "-secrets/gitlab-omnibus-secrets/" $gkms_source ".enc | gcloud --project " .Environment.Values.google_project " kms decrypt --location global --keyring=gitlab-secrets --key " $gkms_source " --ciphertext-file=- --plaintext-file=- | zcat -f")) }}
{{- $dev_gitlab_org_docker_pass := $gkms_omnibus_secrets | exec "jq" (list "-jc" ".gitlab_k8s.\"k8s-workloads-deploy-token\"") }}
{{- $dev_gitlab_org_kas_docker_pass := $gkms_omnibus_secrets | exec "jq" (list "-jc" ".gitlab_k8s.\"k8s-workloads-deploy-token-kas\"") }}
    - resources:
      - apiVersion: v1
        kind: Secret
        type: kubernetes.io/dockerconfigjson
        metadata:
          name: dev-registry-access-v1
          namespace: {{ $namespace }}
        stringData:
          .dockerconfigjson: |
            {"auths":{"dev.gitlab.org:5005":{"username":"k8s-workloads-deploy-token","password":{{ $dev_gitlab_org_docker_pass | quote }},"auth":{{ (list "k8s-workloads-deploy-token:" $dev_gitlab_org_docker_pass) | join "" | b64enc | quote }}}}}
      - apiVersion: v1
        kind: Secret
        type: kubernetes.io/dockerconfigjson
        metadata:
          name: dev-registry-access-kas-v1
          namespace: {{ $namespace }}
        stringData:
          .dockerconfigjson: |
            {"auths":{"dev.gitlab.org:5005":{"username":"k8s-workloads-deploy-token","password":{{ $dev_gitlab_org_kas_docker_pass | quote }},"auth":{{ (list "k8s-workloads-deploy-token:" $dev_gitlab_org_kas_docker_pass) | join "" | b64enc | quote }}}}}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-object-storage-v1
          namespace: {{ $namespace }}
        stringData:
          gitlab-object-storage.yml: |
              provider: Google
              google_project: {{ .Environment.Values.google_project }}
              google_json_key_string: |
{{ ($gkms_omnibus_secrets | exec "jq" (list "-r" ".\"gitlab-server\".\"google-creds\".json_base64")) | b64dec | trimSuffix "\n" | indent 16 }}

{{- $google_provider := $gkms_omnibus_secrets | exec "jq" (list "-jc" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".omniauth_providers[] | select(.name == \"google_oauth2\")") }}
{{- $twitter_provider := $gkms_omnibus_secrets | exec "jq" (list "-jc" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".omniauth_providers[] | select(.name == \"twitter\")") }}
{{- $github_provider := $gkms_omnibus_secrets | exec "jq" (list "-jc" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".omniauth_providers[] | select(.name == \"github\")") }}
{{- $bitbucket_provider := $gkms_omnibus_secrets | exec "jq" (list "-jc" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".omniauth_providers[] | select(.name == \"bitbucket\")") }}
{{- $group_saml_provider := $gkms_omnibus_secrets | exec "jq" (list "-jc" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".omniauth_providers[] | select(.name == \"group_saml\")") }}
{{- $salesforce_provider := $gkms_omnibus_secrets | exec "jq" (list "-jc" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".omniauth_providers[] | select(.name == \"salesforce\")") }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-google-oauth2-v1
          namespace: {{ $namespace }}
        stringData:
          provider:
            {{ $google_provider | quote | nindent 14 }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-twitter-oauth2-v1
          namespace: {{ $namespace }}
        stringData:
          provider:
            {{ $twitter_provider | quote | nindent 14 }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-github-oauth2-v1
          namespace: {{ $namespace }}
        stringData:
          provider:
            {{ $github_provider | quote | nindent 14 }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-bitbucket-oauth2-v1
          namespace: {{ $namespace }}
        stringData:
          provider:
            {{ $bitbucket_provider | quote | nindent 14 }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-group-saml-oauth2-v1
          namespace: {{ $namespace }}
        stringData:
          provider:
            {{ $group_saml_provider | quote | nindent 14 }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-salesforce-oauth2-v1
          namespace: {{ $namespace }}
        stringData:
          provider:
            {{ $salesforce_provider | quote | nindent 14 }}

{{- $registry_authorization_header := $gkms_omnibus_secrets | exec "jq" (list "-jc" "[.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".registry_notification_secret]") }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-registry-authorization-header-v1
          namespace: {{ $namespace }}
        stringData:
          secret:
            {{ $registry_authorization_header | quote | nindent 14 }}

{{- $registry_notification_secret := $gkms_omnibus_secrets | exec "jq" (list "-jc" "[.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".registry_notification_secret]") }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-registry-notification-v1
          namespace: {{ $namespace }}
        stringData:
          secret:
            {{ $registry_notification_secret | quote | nindent 14 }}

      - apiVersion: v1
        kind: Secret
        metadata:
          name: registry-certificate-v1
          namespace: {{ $namespace }}
        stringData:
          registry-auth.crt: |
{{ $gkms_omnibus_secrets | exec "jq" (list "-r" ".\"omnibus-gitlab\".gitlab_rb.registry.internal_certificate") | indent 14 }}
          registry-auth.key: |
{{ $gkms_omnibus_secrets | exec "jq" (list "-r" ".\"omnibus-gitlab\".gitlab_rb.registry.internal_key") | indent 14 }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: registry-storage-v1
          namespace: {{ $namespace }}
        stringData:
          config: |
            gcs:
              bucket: {{ .Environment.Values | getOrNil "registry_bucket_name" | default (printf "gitlab-%s-registry" $env_prefix) }}
              keyfile: /etc/docker/registry/storage/gcs.json
            maintenance:
              uploadpurging:
                enabled: false
            cache:
              blobdescriptor: 'inmemory'
            delete:
              enabled: true
          gcs.json: |
{{ $gkms_omnibus_secrets | exec "jq" (list "-r" ".\"gitlab-server\".\"google-creds\".json_base64") | b64dec | trimSuffix "\n" | indent 14 }}

      # Creates the second version of the registry secret
      #   - Removes the inmemory cache configuration
      - apiVersion: v1
        kind: Secret
        metadata:
          name: registry-storage-v2
          namespace: {{ $namespace }}
        stringData:
          config: |
            gcs:
              bucket: {{ .Environment.Values | getOrNil "registry_bucket_name" | default (printf "gitlab-%s-registry" $env_prefix) }}
              keyfile: /etc/docker/registry/storage/gcs.json
            maintenance:
              uploadpurging:
                enabled: false
            delete:
              enabled: true
          gcs.json: |
{{ $gkms_omnibus_secrets | exec "jq" (list "-r" ".\"gitlab-server\".\"google-creds\".json_base64") | b64dec | trimSuffix "\n" | indent 14 }}

# only add this secret to pre environment for the moment:
{{ if eq (.Environment.Values | getOrNil "env_prefix") "pre" }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: registry-postgresql-password-v1
          namespace: {{ $namespace }}
        stringData:
          password: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.registry.postgres_password") | quote }}
{{ end }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: registry-httpsecret-v1
          namespace: {{ $namespace }}
        stringData:
          secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.registry.http_secret") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-postgres-credential-v1
          namespace: {{ $namespace }}
        stringData:
          secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".db_password") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-redis-credential-v1
          namespace: {{ $namespace }}
        stringData:
          secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".redis_password") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-smtp-credential-v1
          namespace: {{ $namespace }}
        stringData:
          secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".smtp_password") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-workhorse-credential-v1
          namespace: {{ $namespace }}
        stringData:
          secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-workhorse\".secret_token") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-gitaly-credential-v1
          namespace: {{ $namespace }}
        stringData:
          secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".gitaly_token") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-rails-secret-v1
          namespace: {{ $namespace }}
        stringData:
          secrets.yml: |
            production:
              secret_key_base: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".secret_key_base") }}
              otp_key_base: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".otp_key_base") }}
              db_key_base: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".db_key_base") }}
              openid_connect_signing_key: |
{{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".openid_connect_signing_key") | trimSuffix "\n" | indent 16 }}
              ci_jwt_signing_key: |
{{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".ci_jwt_signing_key") | trimSuffix "\n" | indent 16 }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-mailroom-imap-v1
          namespace: {{ $namespace }}
        stringData:
          incoming_email_password: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".incoming_email_password") }}
          service_desk_email_password: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".service_desk_email_password") }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-shell-credential-v1
          namespace: {{ $namespace }}
        stringData:
          secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-shell\".secret_token") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-hostkeys-credential-v1
          namespace: {{ $namespace }}
        stringData:
          ssh_host_ed25519_key: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".ssh.host_keys.ssh_host_ed25519_key") | quote }}
          ssh_host_dsa_key: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".ssh.host_keys.ssh_host_dsa_key") | quote }}
          ssh_host_ecdsa_key: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".ssh.host_keys.ssh_host_ecdsa_key") | quote }}
          ssh_host_rsa_key: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".ssh.host_keys.ssh_host_rsa_key") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-kas-credential-v1
          namespace: {{ $namespace }}
        stringData:
          kas_shared_secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.gitlab_kas.api_secret_key") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-kas-private-api-credential-v1
          namespace: {{ $namespace }}
        stringData:
          kas_private_api_secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".gitlab_k8s[\"kas_private_api_secret\"]") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-pages-api-credential-v1
          namespace: {{ $namespace }}
        stringData:
          shared_secret: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-pages\".api_secret_key") | quote }}
{{ if .Values | getOrNil "gitlab_runner.enabled" | default false }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-runner-registration-credential-v1
          namespace: {{ .Values | getOrNil "gitlab_runner.namespace" | default "gitlab-runner" }}
        stringData:
          runner-registration-token: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".gitlab_k8s[\"gitlab-runner-registration-credential\"]") | quote }}
          runner-token: ""
{{ end }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-gitaly-tls-certificate-v1
          namespace: {{ $namespace }}
        stringData:
          gitaly.crt: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".ssl.trusted_certs[\"gitaly.crt\"]") | quote }}
      - apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-praefect-tls-certificate-v1
          namespace: {{ $namespace }}
        stringData:
          praefect.crt: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".ssl.trusted_certs[\"praefect.crt\"]") | quote }}
