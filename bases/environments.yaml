---
environments:
  # This is a dummy environment used for base values that can
  # be merged into other environments.
  __base:
    values:
      # These defaults are for services that are enabled
      # on all regional clusters, in all environments
      # These are read by values.yaml.gotmpl
      - &regional_cluster_defaults
        # Registry is disabled by default on the regional cluster
        # and enabled selectively for preprod and canary
        registry:
          enabled: false
        gitlab:
          webservice:
            enabled: false
          sidekiq:
            enabled: true
          gitlab-shell:
            enabled: false
          mailroom:
            enabled: true
          kas:
            enabled: true
      # These services are only installed in the
      # regional cluster, so we disable them for
      # all zonal clusters, regardless of the environment.
      - &zonal_cluster_defaults
        registry:
          enabled: true
        gitlab:
          sidekiq:
            enabled: false
          mailroom:
            enabled: false
          gitlab-shell:
            enabled: true
          webservice:
            enabled: true
          kas:
            enabled: false
        gitlab_extras:
          api_no_proxy: true
          ingress_readiness: true

  ops:
    values:
      - <<: *regional_cluster_defaults
      -
        google_project: "gitlab-ops"
        gitlab_endpoint: "https://ops.gitlab.net"
  pre:
    values:
      - <<: *regional_cluster_defaults
      - &pre_values
        env_prefix: pre
        google_project: "gitlab-pre"
        gitlab_domain: "pre.gitlab.com"
        gitlab_endpoint: "https://pre.gitlab.com"
        registry_deployment_for_image_tag: "gitlab-registry"
        registry_api_host: "registry.pre.gitlab.com"
        registry_bucket_name: "gitlab-pre-container-registry"
        registry:
          enabled: true
        gitlab:
          gitlab-shell:
            enabled: true
          webservice:
            enabled: true
        cluster: pre-gitlab-gke
        region: us-east1
        kas:
          managed_cert_name: kas-pre-gitlab-com
          domain: kas.pre.gitlab.com
        gitlab_runner:
          enabled: true
          namespace: gitlab-runner
        gitlab_extras:
          api_no_proxy: true
          ingress_readiness: true
        web:
          enabled: true

  gstg:
    values:
      - <<: *regional_cluster_defaults
      - &gstg_values
        env_prefix: gstg
        google_project: "gitlab-staging-1"
        gitlab_domain: "staging.gitlab.com"
        gitlab_endpoint: "https://staging.gitlab.com"
        gitlab_internal_lb_dns_address: "int.gstg.gitlab.net"
        registry_deployment_for_image_tag: "gitlab-registry"
        registry_api_host: "registry.staging.gitlab.com"
        cluster: gstg-gitlab-gke
        region: us-east1
        kas:
          managed_cert_name: kas-staging-gitlab-com
          domain: kas.staging.gitlab.com
        web:
          enabled: true

  # Zonal clusters for gstg, these three environments
  # inherit from the gstg environemnt, which is the
  # regional cluster
  gstg-us-east1-b:
    values:
      - <<: *regional_cluster_defaults
      - <<: *gstg_values
      - <<: *zonal_cluster_defaults
      -
        cluster: gstg-us-east1-b
        region: us-east1-b

  gstg-us-east1-c:
    values:
      - <<: *regional_cluster_defaults
      - <<: *gstg_values
      - <<: *zonal_cluster_defaults
      -
        cluster: gstg-us-east1-c
        region: us-east1-c

  gstg-us-east1-d:
    values:
      - <<: *regional_cluster_defaults
      - <<: *gstg_values
      - <<: *zonal_cluster_defaults
      -
        cluster: gstg-us-east1-d
        region: us-east1-d

  gprd-cny:
    values:
      - <<: *regional_cluster_defaults
      -
        registry:
          enabled: true
        env_prefix: gprd
        google_project: "gitlab-production"
        gitlab_domain: "gitlab.com"
        gitlab_endpoint: "https://gitlab.com"
        gitlab_internal_lb_dns_address: "int.gprd.gitlab.net"
        gitlab_secrets_gkms_source: "gprd"
        gitlab_release_name: "gitlab-cny"
        gitlab_secrets_release_name: "gitlab-secrets-cny"
        gitlab_secrets_namespace: "gitlab-cny"
        gitlab_namespace: "gitlab-cny"
      # Canary uses the same bucket as production
        registry_bucket_name: "gitlab-gprd-registry"
        registry_deployment_for_image_tag: "gitlab-cny-registry"
        registry_api_host: "registry.gitlab.com"
        gitlab_chef_source_name: "gprd"
        stage: "cny"
        gitlab:
          gitlab-shell:
            enabled: true
          webservice:
            enabled: true
          kas:
            enabled: false
        gitlab_extras:
          api_no_proxy: true
          ingress_readiness: true
          namespace: "gitlab-cny"

  gprd:
    values:
      - <<: *regional_cluster_defaults
      - &gprd_values
        env_prefix: gprd
        google_project: "gitlab-production"
        gitlab_domain: "gitlab.com"
        gitlab_endpoint: "https://gitlab.com"
        gitlab_internal_lb_dns_address: "int.gprd.gitlab.net"
        registry_deployment_for_image_tag: "gitlab-registry"
        registry_api_host: "registry.gitlab.com"
        cluster: gprd-gitlab-gke
        region: us-east1
        kas:
          managed_cert_name: kas-gitlab-com
          domain: kas.gitlab.com

  # Zonal clusters for gprd, these three environments
  # inherit from the gprd environemnt, which is the
  # regional cluster
  gprd-us-east1-b:
    values:
      - <<: *regional_cluster_defaults
      - <<: *gprd_values
      - <<: *zonal_cluster_defaults
      -
        cluster: gprd-us-east1-b
        region: us-east1-b

  gprd-us-east1-c:
    values:
      - <<: *regional_cluster_defaults
      - <<: *gprd_values
      - <<: *zonal_cluster_defaults
      -
        cluster: gprd-us-east1-c
        region: us-east1-c

  gprd-us-east1-d:
    values:
      - <<: *regional_cluster_defaults
      - <<: *gprd_values
      - <<: *zonal_cluster_defaults
      -
        cluster: gprd-us-east1-d
        region: us-east1-d
